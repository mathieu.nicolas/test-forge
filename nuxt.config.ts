import Aura from '@primevue/themes/aura'

export default defineNuxtConfig({
  devtools: { enabled: true },
  nitro: {
    output: {
      publicDir: 'public',
    }
  },
  dir: {
    public: 'www',
  },
  compatibilityDate: '2024-09-17'
})